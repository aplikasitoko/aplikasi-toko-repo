-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.1.9-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win32
-- HeidiSQL Versi:               9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for toko
CREATE DATABASE IF NOT EXISTS `toko` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `toko`;


-- Dumping structure for table toko.barang
CREATE TABLE IF NOT EXISTS `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` text NOT NULL,
  `nama_barang` text NOT NULL,
  `jml_stok` int(3) NOT NULL DEFAULT '0',
  `jml_stok_opname` int(3) NOT NULL DEFAULT '0',
  `harga_beli` int(6) NOT NULL DEFAULT '0',
  `harga_jual` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table toko.barang: ~4 rows (approximately)
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` (`id`, `kode_barang`, `nama_barang`, `jml_stok`, `jml_stok_opname`, `harga_beli`, `harga_jual`) VALUES
	(7, 'Rinso-500gr', 'Rinso 500 gr', 0, 0, 15000, 17000),
	(8, 'Sabun-Lux-100gr', 'Sabun Lux 100gr', 0, 0, 1500, 2000),
	(9, 'Sampo-HS-500gr', 'Sampo Head Solder 500gr', 0, 0, 20000, 22000),
	(10, 'Sampo-Pantene-500gr', 'Sampo Pantene 500gr', 0, 0, 21000, 25000),
	(11, 'Sampo-Sunsulk-500gr', 'Sampo Sunsilk 500gr', 0, 0, 21000, 23000);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;


-- Dumping structure for table toko.distributor
CREATE TABLE IF NOT EXISTS `distributor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_distributor` text NOT NULL,
  `nama_distributor` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table toko.distributor: ~0 rows (approximately)
/*!40000 ALTER TABLE `distributor` DISABLE KEYS */;
INSERT INTO `distributor` (`id`, `kode_distributor`, `nama_distributor`) VALUES
	(2, '001', 'PT ANUGERAH');
/*!40000 ALTER TABLE `distributor` ENABLE KEYS */;


-- Dumping structure for table toko.item_pembelian
CREATE TABLE IF NOT EXISTS `item_pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_nota` varchar(50) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `jml_item` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table toko.item_pembelian: ~0 rows (approximately)
/*!40000 ALTER TABLE `item_pembelian` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_pembelian` ENABLE KEYS */;


-- Dumping structure for table toko.item_penjualan
CREATE TABLE IF NOT EXISTS `item_penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_nota` text NOT NULL,
  `kode_barang` text,
  `jml_item` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table toko.item_penjualan: ~2 rows (approximately)
/*!40000 ALTER TABLE `item_penjualan` DISABLE KEYS */;
INSERT INTO `item_penjualan` (`id`, `kode_nota`, `kode_barang`, `jml_item`, `sub_total`) VALUES
	(12, '979627975', 'Sabun-Lux-100gr', 2, 4000);
/*!40000 ALTER TABLE `item_penjualan` ENABLE KEYS */;


-- Dumping structure for table toko.pembelian
CREATE TABLE IF NOT EXISTS `pembelian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_nota` text NOT NULL,
  `jml_total` int(11) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `id_distributor` text NOT NULL,
  `create_by` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table toko.pembelian: ~0 rows (approximately)
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;
INSERT INTO `pembelian` (`id`, `kode_nota`, `jml_total`, `tanggal_beli`, `id_distributor`, `create_by`) VALUES
	(1, '001', 2000, '2016-05-15', '12', 'User');
/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;


-- Dumping structure for table toko.penjualan
CREATE TABLE IF NOT EXISTS `penjualan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_nota` text NOT NULL,
  `jml_total` int(11) DEFAULT '0',
  `tgl_jual` date DEFAULT NULL,
  `create_by` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table toko.penjualan: ~2 rows (approximately)
/*!40000 ALTER TABLE `penjualan` DISABLE KEYS */;
INSERT INTO `penjualan` (`id`, `kode_nota`, `jml_total`, `tgl_jual`, `create_by`) VALUES
	(21, '979627975', 0, '2016-05-20', 'User');
/*!40000 ALTER TABLE `penjualan` ENABLE KEYS */;


-- Dumping structure for table toko.stok
CREATE TABLE IF NOT EXISTS `stok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` text NOT NULL,
  `jml_stok` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table toko.stok: ~0 rows (approximately)
/*!40000 ALTER TABLE `stok` DISABLE KEYS */;
/*!40000 ALTER TABLE `stok` ENABLE KEYS */;


-- Dumping structure for table toko.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` text NOT NULL,
  `nama` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table toko.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
