<%--
  Created by IntelliJ IDEA.
  User: MAMADCELL
  Date: 19/05/2016
  Time: 09.16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Autocomplete in java web application using Jquery and JSON</title>
    <script src="lib/jquery-1.10.2.js"></script>
    <script src="lib/jquery-ui.js"></script>
    <script src="JS/autocompleter.js"></script>
    <link rel="stylesheet"
          href="lib/jquery-ui.css">
    <!-- User defied css -->
    <link rel="stylesheet" href="style.css">

</head>
<body>
<div class="header">
    <h3>Autocomplete in java web application using Jquery and JSON</h3>
</div>
<br />
<br />
<div class="search-container">
    <div class="ui-widget">
        <input type="text" id="kodebarang" name="search" class="search" />
    </div>
</div>
</body>
</html>