$(document).ready(function() {
    $(function() {
        $("#inIdWilayah").autocomplete({
            source : function(request, response) {
                $.ajax({
                    url : "SearchController",
                    type : "GET",
                    data : {
                        term : request.term
                    },
                    dataType : "json",
                    success : function(data) {
                        response(data);
                    }
                });
            }
        });
    });
});