/**
 * Created by MAMADCELL on 19/05/2016.
 */
function setWilayahUnit() {
    $select = $('#inKodeUnit');

    var varWilayah = $('#inIdWilayah').val();
    var formData = {kodeWilayah: varWilayah};
    $(".wilayahUnit").addClass('ajax_loader');
    //request the JSON data and parse into the select element
    $.ajax({
        url: BASE_URL + "api/master/pengguna/filter_unit_by_wilayah",
        dataType: 'JSON',
        type: "POST",
        data: formData,
        success: function (data) {
//            console.log('data pejabat:' + data);
//            console.log('pejabat', data);
            if (typeof(data) === "undefined" || data === null) {
                $select.html('<option value="">none available</option>');
                $(".wilayahUnit").removeClass('ajax_loader');
                return;
            }

            $select.html('');
            $select.append('<option value=""> -- Pilih Unit -- </option>');
            $.each(data, function (key, val) {
                $select.append('<option value="' + val.id + '">' + val.value + '</option>');
            });
            $(".wilayahUnit").removeClass('ajax_loader');


        },
        error: function () {
            $select.html('<option value="">none available</option>');
        }
    });}
