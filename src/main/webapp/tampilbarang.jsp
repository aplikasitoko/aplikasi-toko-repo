<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>All Posts</title>
</head>
<body>
<div style="width: 1200px; margin-left: auto; margin-right: auto;">
    <sql:setDataSource
            var="myDS"
            driver="com.mysql.jdbc.Driver"
            url="jdbc:mysql://localhost:3306/toko"
            user="root" password="root"
            />

    <sql:query var="list"   dataSource="${myDS}">
        SELECT * FROM barang;
    </sql:query>
    <table border="9" cellpadding="10">
        <tr>
            <th>KODE</th>
            <th>NAMA BARANG</th>
            <th>JML STOK</th>
            <th>JML STOK OPNAME</th>
            <th>HARGA BELI</th>
            <th>HARGA JUAL</th>
            <th>AKSI</th>
        </tr>
        <c:forEach var="barang" items="${list.rows}">
            <tr>
                <td><c:out value="${barang.kode_barang}" /></td>
                <td><c:out value="${barang.nama_barang}" /></td>
                <td><c:out value="${barang.jml_stok}" /></td>
                <td><c:out value="${barang.jml_stok_opname}" /></td>
                <td><c:out value="${barang.harga_beli}" /></td>
                <td><c:out value="${barang.harga_jual}" /></td>
                <td><a href="editbarang?id=${barang.id}">Edit</a>
                    <a href="deletebarang?id=${barang.id}">Delete</a></td>
            </tr>
        </c:forEach>
        <p> </p>
    </table>
</div>
</body>
</html>