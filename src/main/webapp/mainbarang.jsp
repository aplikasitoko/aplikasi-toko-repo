<%--
  Created by IntelliJ IDEA.
  User: MAMADCELL
  Date: 17/05/2016
  Time: 08.46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Aplikasi Toko</title>
   <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="header">

  <div id="menu">
    <div class="left"><ul>
      <li><a href=mainbarang.jsp>INPUT BARANG</a></li>
      <li><a href=mainpembelian.jsp>INPUT PEMBELIAN</a></li>
      <li><a href=mainpenjualan.jsp>INPUT PENJUALAN</a></li>
    </ul>
    </div>
    <div class="right">
      <ul class="topmenu">
           <li><a href=logout.php>Logout</a></li>
      </ul>
    </div>
  </div>
</div>
<div id="wrap">
  <div id="content">
    <a href="inputbarang.jsp">Tambah</a>
    <%@include file="tampilbarang.jsp" %>

  </div>

  <div id="footer">
    Copyright
  </div>
</div>
</body>
</html>
