<%--
  Created by IntelliJ IDEA.
  User: MAMADCELL
  Date: 18/05/2016
  Time: 13.10
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>All Posts</title>
</head>
<body>
<div style="width: 1200px; margin-left: auto; margin-right: auto;">
  <sql:setDataSource
          var="myDS"
          driver="com.mysql.jdbc.Driver"
          url="jdbc:mysql://localhost:3306/toko"
          user="root" password="root"
          />

  <sql:query var="list"   dataSource="${myDS}">
    SELECT * FROM penjualan;
  </sql:query>
  <table border="9" cellpadding="10">
    <tr>
      <th>KODE NOTA</th>
      <th>JML TOTAL</th>
      <th>TANGGAL JUAL</th>
      <th>CREATE BY</th>
       <th>AKSI</th>
    </tr>
    <c:forEach var="penjualan" items="${list.rows}">
      <tr>
        <td><c:out value="${penjualan.kode_nota}" /></td>
        <td><c:out value="${penjualan.jml_total}" /></td>
        <td><c:out value="${penjualan.tgl_jual}" /></td>
        <td><c:out value="${penjualan.create_by}" /></td>
           <td><a href="editjual?id=${penjualan.kode_nota}">Edit</a>
          <a href="deletejual?id=${penjualan.kode_nota}">Delete</a></td>
      </tr>
    </c:forEach>
    <p> </p>
  </table>
</div>
</body>
</html>