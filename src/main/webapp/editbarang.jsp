<%--
  Created by IntelliJ IDEA.
  User: MAMADCELL
  Date: 17/05/2016
  Time: 15.13
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Aplikasi Toko</title>
  <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="header"></div>
<h1>Edit Barang</h1>
<a href="mainbarang.jsp">BACK</a>
  <div id="wrap">
        <div id="content">
  <c:forEach items="${getBarangById}" var="p">
    <form action="JSP/ManagerEditBarang.jsp" method="post">
      <input type="hidden" name="id" value="${p.id}">
      Kode Barang:<br>
      <input type="text" value="${p.kodeBarang}" name="kodebarang" style="width: 200px"><br>
      Nama Barang:<br>
      <input type="text" value="${p.namaBarang}" name="namabarang" style="width: 200px"><br>
      Jml Stok:<br>
      <input type="text" value="${p.jmlStok}" name="jmlstok" style="width: 200px"><br>
      Jml Stok Opname:<br>
      <input type="text" value="${p.jmlStokOpname}" name="jmlstokopname" style="width: 200px"><br>
      Harga Beli:<br>
      <input type="text" value="${p.hargaBeli}" name="hargabeli" style="width: 200px"><br>
      Harga Jual:<br>
      <input type="text" value="${p.hargaJual}" name="hargajual" style="width: 200px"><br>
      <input type="submit" value="Submit"><input type="reset" value="Reset">
    </form>
  </c:forEach>
</div></div>
    <div id="footer">
      Copyright
      </div>
</body>
</html>
