<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Date"%>
<%@ page import="com.aplikasi.dao.BarangDao" %>
<%@ page import="com.aplikasi.entiti.Barang" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
</head>
<body>
<%

    String idTemp = request.getParameter("id");
    System.out.println("ID ------="+idTemp);
    int id = Integer.parseInt(idTemp);
    Barang barang=new Barang();
    barang.setId(id);
    barang.setKodeBarang(request.getParameter("kodebarang"));
    barang.setNamaBarang(request.getParameter("namabarang"));
    barang.setJmlStok(Integer.valueOf(request.getParameter("jmlstok")));
    barang.setJmlStokOpname(Integer.valueOf(request.getParameter("jmlstokopname")));
    barang.setHargaBeli(Integer.valueOf(request.getParameter("hargabeli")));
    barang.setHargaJual(Integer.valueOf(request.getParameter("hargajual")));

    BarangDao da = new BarangDao();
    da.edit(barang);

    response.sendRedirect("/mainbarang.jsp");
%>
</body>
</html>