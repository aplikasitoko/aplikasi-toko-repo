package com.aplikasi.entiti;

import java.util.Date;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class Penjualan {
    private Integer id;
    private String kodeNota;
    private Integer jmlTotal;
    private Date tanggalJual;
    private String createBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeNota() {
        return kodeNota;
    }

    public void setKodeNota(String kodeNota) {
        this.kodeNota = kodeNota;
    }

    public Integer getJmlTotal() {      return jmlTotal;    }

    public void setJmlTotal(Integer jmlTotal) {        this.jmlTotal = jmlTotal;    }

    public Date getTanggalJual() {
        return tanggalJual;
    }

    public void setTanggalJual(Date tanggalJual) {
        this.tanggalJual = tanggalJual;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
    public String toString() {
        return "Penjualan{" +
                "id=" + id +
                ", kodeNota='" + kodeNota + '\'' +
                ", jmlTotal='" + jmlTotal + '\'' +
                ", tanggalJual=" + tanggalJual +
                ", createBy='" + createBy + '\'' +
                '}';
    }
}
