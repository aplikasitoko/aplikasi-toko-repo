package com.aplikasi.entiti;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class User {
    private Integer id;
    private String kodeUser;
    private String nama;

    public String getKodeUser() {
        return kodeUser;
    }

    public void setKodeUser(String kodeUser) {
        this.kodeUser = kodeUser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

        public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", kodeUser='" + kodeUser + '\'' +
                ", nama='" + nama + '\'' +
                '}';
    }
}
