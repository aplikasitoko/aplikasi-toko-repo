package com.aplikasi.entiti;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class Distributor {

    private Integer id;
    private String kodeDistributor;
    private String namaDistributor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeDistributor() {
        return kodeDistributor;
    }

    public void setKodeDistributor(String kodeDistributor) {
        this.kodeDistributor = kodeDistributor;
    }

    public String getNamaDistributor() {
        return namaDistributor;
    }

    public void setNamaDistributor(String namaDistributor) {
        this.namaDistributor = namaDistributor;
    }

    @Override
    public String toString() {
        return "Distributor{" +
                "id=" + id +
                ", kodeDistributor='" + kodeDistributor + '\'' +
                ", namaDistributor='" + namaDistributor + '\'' +
                '}';
    }
}
