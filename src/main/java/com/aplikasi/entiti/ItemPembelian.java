package com.aplikasi.entiti;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class ItemPembelian {
    private Integer id;
    private String kodeNota;
    private String kodeBarang;
    private Integer jmlItem;
    private Integer subTotal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeNota() {
        return kodeNota;
    }

    public void setKodeNota(String kodeNota) {
        this.kodeNota = kodeNota;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public Integer getJmlItem() {
        return jmlItem;
    }

    public void setJmlItem(Integer jmlItem) {
        this.jmlItem = jmlItem;
    }

    public Integer getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Integer subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return "ItemPembelian{" +
                "id=" + id +
                ", kodeNota='" + kodeNota + '\'' +
                ", kodeBarang='" + kodeBarang + '\'' +
                ", jmlItem=" + jmlItem +
                ", subTotal=" + subTotal +
                '}';
    }
}
