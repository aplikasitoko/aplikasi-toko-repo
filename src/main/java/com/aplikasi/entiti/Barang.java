package com.aplikasi.entiti;

/**
 * Created by MAMADCELL on 13/05/2016.
 */

import java.util.Date;

public class Barang {
    private Integer id;
    private String kodeBarang;
    private String namaBarang;
    private Integer jmlStok;
    private Integer jmlStokOpname;
    private Integer hargaBeli;
    private Integer hargaJual;

//    public Barang(int id, String kodeBarang, String namaBarang, int jmlStok, int jmlStokOpname, int hargaBeli,int hargaJual) {
//        this.id = id;
//        this.kodeBarang = kodeBarang;
//        this.namaBarang = namaBarang;
//        this.jmlStok = jmlStok;
//        this.jmlStokOpname = jmlStokOpname;
//        this.hargaBeli = hargaBeli;
//        this.hargaJual = hargaJual;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public Integer getJmlStok() {
        return jmlStok;
    }

    public void setJmlStok(Integer jmlStok) {
        this.jmlStok = jmlStok;
    }

    public Integer getJmlStokOpname() {
        return jmlStokOpname;
    }

    public void setJmlStokOpname(Integer jmlStokOpname) {
        this.jmlStokOpname = jmlStokOpname;
    }

    public Integer getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Integer hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public Integer getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(Integer hargaJual) {
        this.hargaJual = hargaJual;
    }

    }















