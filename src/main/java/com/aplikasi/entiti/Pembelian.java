package com.aplikasi.entiti;

import java.util.Date;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class Pembelian {
    private Integer id;
    private String kodeNota;
    private Integer jmlTotal;
    private Date tanggalBeli;
    private String kodeDistributor;
    private String createBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodeNota() {
        return kodeNota;
    }

    public void setKodeNota(String kodeNota) {
        this.kodeNota = kodeNota;
    }

    public Integer getJmlTotal() {
        return jmlTotal;
    }

    public void setJmlTotal(Integer jmlTotal) {
        this.jmlTotal = jmlTotal;
    }

    public Date getTanggalBeli() {
        return tanggalBeli;
    }

    public void setTanggalBeli(Date tanggalBeli) {
        this.tanggalBeli = tanggalBeli;
    }

    public String getKodeDistributor() {
        return kodeDistributor;
    }

    public void setKodeDistributor(String kodeDistributor) {
        this.kodeDistributor = kodeDistributor;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
    public String toString() {
        return "Pembelian{" +
                "id=" + id +
                ", kodeNota='" + kodeNota + '\'' +
                ", jmlTotal=" + jmlTotal +
                ", tanggalBeli=" + tanggalBeli +
                ", kodeDistributor='" + kodeDistributor + '\'' +
                ", createBy='" + createBy + '\'' +
                '}';
    }
}
