package com.aplikasi.servlet;

import com.aplikasi.dao.ItemPembelianDao;
import com.aplikasi.entiti.ItemPembelian;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by MAMADCELL on 14/05/2016.
 */
public class FormInputItemPembelianServlet extends HttpServlet {
    public ItemPembelianDao itemPembelianDao=new ItemPembelianDao();

    public void doGet(HttpServletRequest req,HttpServletResponse res){
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res){
        try {
            ItemPembelian itemPembelian = new ItemPembelian();
            itemPembelian.setKodeNota(req.getParameter("kodenota"));
            itemPembelian.setKodeBarang(req.getParameter("kodebarang"));
            itemPembelian.setJmlItem(Integer.valueOf(req.getParameter("jmlitem")));
            itemPembelian.setSubTotal(Integer.valueOf(req.getParameter("jmlsubtotal")));
            itemPembelianDao.simpan(itemPembelian);
            res.sendRedirect("itempembelian.jsp");
        }catch (Exception err) {
            err.printStackTrace();
        }
    }
}
