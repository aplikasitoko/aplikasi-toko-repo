package com.aplikasi.servlet;


import com.aplikasi.dao.DistributorDao;
import com.aplikasi.entiti.Distributor;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class FormInputDistributorServlet extends HttpServlet {

    public DistributorDao distributorDao=new DistributorDao();

    public void doGet(HttpServletRequest req,HttpServletResponse res){
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res){
        try {
            Distributor distributor = new Distributor();
            distributor.setKodeDistributor(req.getParameter("kodedistributor"));
            distributor.setNamaDistributor(req.getParameter("namadistributor"));
            distributorDao.simpan(distributor);
            res.sendRedirect("distributor.jsp");
        }catch (Exception err) {
            err.printStackTrace();
        }
    }

}
