package com.aplikasi.servlet;

import com.aplikasi.dao.ItemPembelianDao;
import com.aplikasi.dao.ItemPenjualanDao;
import com.aplikasi.dao.PenjualanDao;
import com.aplikasi.entiti.ItemPembelian;
import com.aplikasi.entiti.ItemPenjualan;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by MAMADCELL on 14/05/2016.
 */
public class FormInputItemPenjualanServlet extends HttpServlet{
    public ItemPenjualanDao itemPenjualanDao=new ItemPenjualanDao();

    public void doGet(HttpServletRequest req,HttpServletResponse res){
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res){
        try {
            ItemPenjualan itemPenjualan = new ItemPenjualan();
            itemPenjualan.setKodeNota(req.getParameter("kodenota"));
            itemPenjualan.setKodeBarang(req.getParameter("kodebarang"));
            itemPenjualan.setJmlItem(Integer.valueOf(req.getParameter("jmlitem")));
            itemPenjualan.setSubTotal(Integer.valueOf(req.getParameter("jmlsubtotal")));
            itemPenjualanDao.simpan(itemPenjualan);
            String id =(itemPenjualan.getKodeNota());
            req.setAttribute("getPenjualanByNota", PenjualanDao.getPenjualanByNota(id));
            RequestDispatcher rd = req.getRequestDispatcher("inputitempenjualan.jsp");
            //res.sendRedirect("inputitempenjualan.jsp");
            rd.forward(req, res);
        }catch (Exception err) {
            err.printStackTrace();
        }
    }

}
