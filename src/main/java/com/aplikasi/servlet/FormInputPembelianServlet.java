package com.aplikasi.servlet;

import com.aplikasi.dao.ItemPenjualanDao;
import com.aplikasi.dao.PembelianDao;
import com.aplikasi.entiti.ItemPenjualan;
import com.aplikasi.entiti.Pembelian;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by MAMADCELL on 14/05/2016.
 */
public class FormInputPembelianServlet extends HttpServlet {

    public PembelianDao pembelianDao=new PembelianDao();

    public void doGet(HttpServletRequest req,HttpServletResponse res){
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res){
        try {
            Pembelian pembelian = new Pembelian();
            pembelian.setKodeNota(req.getParameter("kodenota"));
            pembelian.setJmlTotal(Integer.valueOf(req.getParameter("jmltotal")));
            Date dateobjek= new Date();
            //Date strTanggalLahir = dateobjek;
            SimpleDateFormat formatterTanggal = new SimpleDateFormat("dd-MM-yyyy");
            formatterTanggal.format(dateobjek);
            pembelian.setTanggalBeli(dateobjek);
            pembelian.setKodeDistributor(req.getParameter("kodedistributor"));
            pembelian.setCreateBy("User");
            pembelianDao.simpan(pembelian);
            res.sendRedirect("inputpembelian.jsp");
        }catch (Exception err) {
            err.printStackTrace();
        }
    }
}
