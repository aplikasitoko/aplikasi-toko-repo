package com.aplikasi.servlet;

/**
 * Created by MAMADCELL on 19/05/2016.
 */

import com.aplikasi.dao.BarangDao;
import com.aplikasi.dao.ItemPenjualanDao;
import com.aplikasi.dao.PenjualanDao;
import com.aplikasi.entiti.Penjualan;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeletePenjualan extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idTemp = request.getParameter("id");
        System.out.println("KODE NOTA TEMP------> "+idTemp);
        //int id = Integer.parseInt(idTemp);
        ItemPenjualanDao itemPenjualanDao=new ItemPenjualanDao();
        PenjualanDao penjualanDao=new PenjualanDao();
        //penjualanDao.getPenjualanByid(id);
       // System.out.println("KODE NOTA ITEM------> "+penjualanDao);

        //itemPenjualanDao.getItemPenjualanByNota(kodeNota);
        //System.out.println("KODE NOTA------> "+id);
        penjualanDao.deleteByNota(idTemp);
        itemPenjualanDao.deletebynota(idTemp);

        response.sendRedirect("mainpenjualan.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}