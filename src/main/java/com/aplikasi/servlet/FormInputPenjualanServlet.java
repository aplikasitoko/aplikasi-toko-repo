package com.aplikasi.servlet;

import com.aplikasi.dao.BarangDao;
import com.aplikasi.dao.PembelianDao;
import com.aplikasi.dao.PenjualanDao;
import com.aplikasi.entiti.ItemPenjualan;
import com.aplikasi.entiti.Pembelian;
import com.aplikasi.entiti.Penjualan;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by MAMADCELL on 15/05/2016.
 */
public class FormInputPenjualanServlet extends HttpServlet {
    public PenjualanDao penjualanDao=new PenjualanDao();

    public void doGet(HttpServletRequest req,HttpServletResponse res){
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res){
        try {
            Penjualan penjualan = new Penjualan();
//            penjualan.setId(Integer.valueOf(req.getParameter("id")));
            penjualan.setKodeNota(req.getParameter("kodenota"));
            penjualan.setJmlTotal(Integer.valueOf(req.getParameter("jmltotal")));
            Date dateobjek= new Date();
            //Date strTanggalLahir = dateobjek;
            SimpleDateFormat formatterTanggal = new SimpleDateFormat("dd-MM-yyyy");
            formatterTanggal.format(dateobjek);
            penjualan.setTanggalJual(dateobjek);
            penjualan.setCreateBy("User");
            penjualanDao.simpan(penjualan);
            //ItemPenjualan itemPenjualan=new ItemPenjualan();
            //itemPenjualan.setKodeNota(penjualan.getKodeNota());
            String id =(penjualan.getKodeNota());
            req.setAttribute("getPenjualanByNota", PenjualanDao.getPenjualanByNota(id));
            RequestDispatcher rd = req.getRequestDispatcher("inputitempenjualan.jsp");
            //res.sendRedirect("inputitempenjualan.jsp");
            rd.forward(req, res);
        }catch (Exception err) {
            err.printStackTrace();
        }
    }
}
