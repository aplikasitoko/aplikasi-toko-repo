package com.aplikasi.servlet;


/**
 * Created by MAMADCELL on 13/05/2016.
 */
        import com.aplikasi.dao.BarangDao;
        import com.aplikasi.entiti.Barang;

        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;


public class FormInputBarangServlet extends HttpServlet {

    public BarangDao barangDao=new BarangDao();

    public void doGet(HttpServletRequest req,HttpServletResponse res){
    }

    public void doPost(HttpServletRequest req,HttpServletResponse res){
        try {
            Barang barang = new Barang();
            barang.setKodeBarang(req.getParameter("kodebarang"));
            barang.setNamaBarang(req.getParameter("namabarang"));
            barang.setJmlStok(Integer.valueOf(req.getParameter("jmlstok")));
            barang.setJmlStokOpname(Integer.valueOf(req.getParameter("jmlstokopname")));
            barang.setHargaBeli(Integer.valueOf(req.getParameter("hargabeli")));
            barang.setHargaJual(Integer.valueOf(req.getParameter("hargajual")));
            barangDao.simpan(barang);
            res.sendRedirect("mainbarang.jsp");
        }catch (Exception err) {
            err.printStackTrace();
        }
    }
}

