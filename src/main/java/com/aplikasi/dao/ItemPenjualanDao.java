package com.aplikasi.dao;

import com.aplikasi.entiti.ItemPenjualan;
import com.aplikasi.util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by MAMADCELL on 14/05/2016.
 */
public class ItemPenjualanDao {


    public void simpan(ItemPenjualan itemPenjualan){
        try {
            PreparedStatement ps = DBUtils.getPreparedStatement("insert into item_penjualan (kode_nota,kode_barang,jml_item,sub_total)values(?,?,?,?)");
            ps.setString(1, itemPenjualan.getKodeNota());
            ps.setString(2, itemPenjualan.getKodeBarang());
            ps.setInt(3, itemPenjualan.getJmlItem());
            ps.setInt(4, itemPenjualan.getSubTotal());
            //ps.setDate(4, new java.sql.Date(p.getTanggalLahir().getTime()));
            ps.executeUpdate();
        } catch (Exception err){
            err.printStackTrace();
        }
    }
    public static List<ItemPenjualan> getItemPenjualanByNota(String id){
        System.out.println(id);
        List<ItemPenjualan> ls = new LinkedList<ItemPenjualan>();
        String sql = "select kode_nota from penjualan where kode_nota = " +id;
        try {
            ResultSet rs = DBUtils.getPreparedStatement(sql).executeQuery();
            while(rs.next()){
                ItemPenjualan itemPenjualan = new ItemPenjualan();
                itemPenjualan.setId(rs.getInt("id"));
                itemPenjualan.setKodeNota(rs.getString("kode_nota"));
                System.out.println(itemPenjualan.getId());
                System.out.println(itemPenjualan.getKodeNota());
                ls.add(itemPenjualan);
                System.out.println(ls);
            }
        } catch (Exception ex) {
            Logger.getLogger(BarangDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ls;
    }

    public void deletebynota(String id){
        try {
            String sql = "delete from item_penjualan where kode_nota = ?";
            PreparedStatement ps = DBUtils.getPreparedStatement(sql);
            ps.setString(1, id);

            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(PenjualanDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



}
