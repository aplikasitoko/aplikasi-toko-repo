package com.aplikasi.dao;

import com.aplikasi.entiti.Distributor;
import com.aplikasi.util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by MAMADCELL on 13/05/2016.
 */
public class DistributorDao {


    public void simpan(Distributor distributor){
        try {
            PreparedStatement ps = DBUtils.getPreparedStatement("insert into distributor (kode_distributor,nama_distributor)values(?,?)");
            ps.setString(1, distributor.getKodeDistributor());
            ps.setString(2, distributor.getNamaDistributor());
           //ps.setDate(4, new java.sql.Date(p.getTanggalLahir().getTime()));
            ps.executeUpdate();
            } catch (Exception err){
            err.printStackTrace();
        }
    }
}
