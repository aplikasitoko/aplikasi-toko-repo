package com.aplikasi.dao;


/**
 * Created by MAMADCELL on 13/05/2016.
 */

import com.aplikasi.entiti.Barang;
import com.aplikasi.util.DBUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BarangDao {

     public void simpan(Barang barang){
        try {
            PreparedStatement ps = DBUtils.getPreparedStatement("insert into barang (kode_barang,nama_barang,jml_stok,jml_stok_opname,harga_beli,harga_jual)values(?,?,?,?,?,?)");
            System.out.println(barang.getKodeBarang());
            ps.setString(1, barang.getKodeBarang());
            ps.setString(2, barang.getNamaBarang());
            ps.setInt(3, barang.getJmlStok());
            ps.setInt(4, barang.getJmlStokOpname());
            ps.setInt(5, barang.getHargaBeli());
            ps.setInt(6, barang.getHargaJual());
            //ps.setDate(4, new java.sql.Date(p.getTanggalLahir().getTime()));
            ps.executeUpdate();

        } catch (Exception err){
            err.printStackTrace();
        }
    }
    public  ArrayList<String> tampilbyid(String id  ){
Barang barang=new Barang();
            //Koneksi koneksi=new Koneksi();
            ArrayList<String> listBarang = new ArrayList<String>();
        try {
            System.out.println(barang.getKodeBarang());
               String sql = "select * from barang where kode_barang like ?";
              PreparedStatement ps = null;
               ps=DBUtils.getPreparedStatement(sql);
ps.setString(1,id +"%");
            ResultSet rs = ps.executeQuery();
                  while(rs.next()) {
                    barang.setKodeBarang(rs.getString("kode_barang"));
                      barang.setHargaJual(rs.getInt("harga_jual"));
                    listBarang.add(barang.getKodeBarang());
                      listBarang.add(barang.getHargaJual().toString());

                }
        } catch (Exception err){
            err.printStackTrace();
        }
return listBarang;
    }


    public void edit(Barang barang){
        try {
            String sql = "update barang SET kode_barang = ?, nama_barang = ?, jml_stok = ?, jml_stok_opname = ?, harga_beli = ?,harga_jual=?" + " where id = ?";
            PreparedStatement ps= DBUtils.getPreparedStatement(sql);
            ps.setString(1, barang.getKodeBarang());
            ps.setString(2, barang.getNamaBarang());
            ps.setInt(3, barang.getJmlStok());
            ps.setInt(4, barang.getJmlStokOpname());
            ps.setInt(5, barang.getHargaBeli());
            ps.setInt(6, barang.getHargaJual());
            ps.setInt(7, barang.getId());
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(BarangDao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }



    public void delete(int id){
             try {
                 String sql = "delete from barang where id = ?";
                 PreparedStatement ps = DBUtils.getPreparedStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(BarangDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public static List<Barang> getBarangById(String id){
        System.out.println(id);
        List<Barang> ls = new LinkedList<Barang>();
        String sql = "select * from barang where id = " +id;
        try {
            ResultSet rs = DBUtils.getPreparedStatement(sql).executeQuery();
            while(rs.next()){
                Barang barang = new Barang();
                barang.setId(rs.getInt("id"));
                barang.setKodeBarang(rs.getString("kode_barang"));
                barang.setNamaBarang(rs.getString("nama_barang"));
                barang.setJmlStok(rs.getInt("jml_stok"));
                barang.setJmlStokOpname(rs.getInt("jml_stok_opname"));
                barang.setHargaBeli(rs.getInt("harga_beli"));
                barang.setHargaJual(rs.getInt("harga_jual"));
                System.out.println(barang.getId());
                System.out.println(barang.getKodeBarang());
                ls.add(barang);
                System.out.println(ls);
            }
        } catch (Exception ex) {
            Logger.getLogger(BarangDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ls;

    }




}





