package com.aplikasi.dao;

import com.aplikasi.entiti.Penjualan;
import com.aplikasi.util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by MAMADCELL on 15/05/2016.
 */
public class PenjualanDao {


    public void simpan(Penjualan penjualan){
        try {
            System.out.println(penjualan.getKodeNota());
            PreparedStatement ps = DBUtils.getPreparedStatement("insert into penjualan (kode_nota,jml_total,tgl_jual,create_by)values(?,?,?,?)");
            ps.setString(1, penjualan.getKodeNota());
            ps.setInt(2, penjualan.getJmlTotal());
            ps.setDate(3, new java.sql.Date(penjualan.getTanggalJual().getTime()));
            ps.setString(4, penjualan.getCreateBy());
            //ps.setDate(4, new java.sql.Date(p.getTanggalLahir().getTime()));
            ps.executeUpdate();
        } catch (Exception err){
            err.printStackTrace();
        }
    }
    public static List<Penjualan> getPenjualanByNota(String id){
        System.out.println("getPenjualanByNota----->"+id);
        List<Penjualan> ls = new LinkedList<Penjualan>();
        String sql = "select * from penjualan where kode_nota = " +id;
        try {
            ResultSet rs = DBUtils.getPreparedStatement(sql).executeQuery();
            while(rs.next()){
                Penjualan penjualan = new Penjualan();
                penjualan.setId(rs.getInt("id"));
                penjualan.setKodeNota(rs.getString("kode_nota"));
                System.out.println("Id Penjualan"+penjualan.getId());
                System.out.println("Kode Nota"+penjualan.getKodeNota());
                ls.add(penjualan);
                System.out.println(ls);
            }
        } catch (Exception ex) {
            Logger.getLogger(BarangDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ls;
    }

    public static List<Penjualan> getPenjualanByid(String id){
        System.out.println("getPenjualanById----->"+id);
        List<Penjualan> ls = new LinkedList<Penjualan>();
        String sql = "select * from penjualan where id = " +id;
        try {
            ResultSet rs = DBUtils.getPreparedStatement(sql).executeQuery();
            while(rs.next()){
                Penjualan penjualan = new Penjualan();
                penjualan.setId(rs.getInt("id"));
                penjualan.setKodeNota(rs.getString("kode_nota"));
                System.out.println("Id Penjualan"+penjualan.getId());
                System.out.println("Kode Nota"+penjualan.getKodeNota());
                ls.add(penjualan);
                System.out.println(ls);
            }
        } catch (Exception ex) {
            Logger.getLogger(BarangDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ls;
    }

    public void deleteByNota(String id){
        try {
            String sql = "delete from penjualan where kode_nota = ?";
            PreparedStatement ps = DBUtils.getPreparedStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            Logger.getLogger(PenjualanDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
