package com.aplikasi.dao;

import com.aplikasi.entiti.ItemPembelian;
import com.aplikasi.util.DBUtils;


import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by MAMADCELL on 14/05/2016.
 */
public class ItemPembelianDao {

    public void simpan(ItemPembelian itemPembelian){
        try {
            PreparedStatement ps = DBUtils.getPreparedStatement("insert into item_pembelian (kode_nota,kode_barang,jml_item,sub_total)values(?,?,?,?)");
            ps.setString(1, itemPembelian.getKodeNota());
            ps.setString(2, itemPembelian.getKodeBarang());
            ps.setInt(3, itemPembelian.getJmlItem());
            ps.setInt(4, itemPembelian.getSubTotal());
            //ps.setDate(4, new java.sql.Date(p.getTanggalLahir().getTime()));
            ps.executeUpdate();
             } catch (Exception err){
            err.printStackTrace();
        }
    }
}
