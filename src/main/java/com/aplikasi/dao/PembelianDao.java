package com.aplikasi.dao;

import com.aplikasi.entiti.Pembelian;
import com.aplikasi.util.DBUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by MAMADCELL on 14/05/2016.
 */
public class PembelianDao {


    public void simpan(Pembelian pembelian){
        try {

            System.out.println(pembelian.getKodeNota());
            PreparedStatement ps = DBUtils.getPreparedStatement("insert into pembelian (kode_nota,jml_total,tanggal_beli,id_distributor,create_by)values(?,?,?,?,?)");
            ps.setString(1, pembelian.getKodeNota());
            ps.setInt(2, pembelian.getJmlTotal());
            ps.setDate(3, new java.sql.Date(pembelian.getTanggalBeli().getTime()));
            ps.setString(4, pembelian.getKodeDistributor());
            ps.setString(5, pembelian.getCreateBy());

            //ps.setDate(4, new java.sql.Date(p.getTanggalLahir().getTime()));
            ps.executeUpdate();

        } catch (Exception err){
            err.printStackTrace();
        }
    }
}
