package com.aplikasi.controller;

/**
 * Created by MAMADCELL on 19/05/2016.
 */
        import java.io.IOException;
        import java.util.ArrayList;

        import javax.servlet.ServletException;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;

        import com.aplikasi.dao.BarangDao;
        import com.google.gson.Gson;

public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest req,
                         HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("application/json");
        try {
            String term = req.getParameter("term");
            System.out.println("Data from ajax call " + term);

            BarangDao dataDao = new BarangDao();
            ArrayList<String> list = dataDao.tampilbyid(term);

            String searchList = new Gson().toJson(list);
            res.getWriter().write(searchList);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
