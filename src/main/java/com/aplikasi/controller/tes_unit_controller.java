package com.aplikasi.controller;

import com.aplikasi.dao.BarangDao;
import com.aplikasi.entiti.Barang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MAMADCELL on 19/05/2016.
 */
@Controller
public class tes_unit_controller {


    @RequestMapping(value = "api/master/pengguna/filter_unit_by_wilayah", method = RequestMethod.POST)
    @ResponseBody
    public List<Barang> filterUnitByWilayah(ModelMap modelMap,
                                          @RequestParam(value = "kodeBarang", defaultValue = "") String kodeBarang,
                                          HttpServletRequest request
    ) {
        //Map<String, Object> map = new HashMap<>();

        //ListUnit l = new ListUnit();
        ArrayList<Barang> harga=new ArrayList<Barang>();
        try {

            // JenisPengadaan jp = new JenisPengadaan();


            System.out.println("tess : " + kodeBarang);

                harga = (ArrayList<Barang>) BarangDao.getBarangById(kodeBarang);

        } catch (Exception e2) {
            e2.printStackTrace();
        }

        return harga;

    }
}